# Configuration file for RST preview.

import os
import sys

# Extensions
sys.path.append(os.path.abspath('exts'))
extensions = [
    'reference',
    'peertube',
    'sphinx.ext.mathjax',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
]

# Project
project = 'projects.blender.org'
root_doc = 'contents'

# Theme: epub hides all navigation, sidebars, footers.
html_theme = 'epub'
html_permalinks = False
